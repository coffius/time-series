package io.koff.timeseries.optimized

case class Range(left: Int, right: Int)
